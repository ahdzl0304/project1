# Project 1 : Catch and Match #

20150822 Hwang Youngzu  // 2020.05.31

bitbicket link : https://bitbucket.org/ahdzl0304/project1/src/master/


-----
Simple board game which is knows as Mahjong. This game is a simpler one than mahjong, but more dynamic and fun! 
To catch cards and match with others to avoid obstacles, you can train your brain with enjoyable rules. 

Catch it! and Match them!

-----

## Game Rules
### Cards
![Alt text](Project1_Code/data/cardBlanked.png)


Blank Card : If there is no picture on it, it represents obstacles that cannot be chosen and passed.

![Alt text](Project1_Code/data/cardFilled.png)


Filled Card : If there is a picture(fruits or vegetables), it represts real cards that you need to delete all for clearing stages.

### How to match
    
**Basic rules** 

You need to match two same cards with same pictures. Also, there are some rules for card pair's positions. First legal match is that 2 cards are adjacent with sharing one side. Other match is that you need to find appropriate route for connection between two cards. The route needs to be coposed of connected horizontal or vertical lines. Also, during the route, line can be bent at most 2 times. In other words, if you can match 2 cards to make a route composed of connected 3 or less horizontal lines or vertical lines.


**Legal Example**

![Alt text](Project1_Code/data/example1.png)



![Alt text](Project1_Code/data/example2.png)


![Alt text](Project1_Code/data/example3.png)
To use less than 3 lines, and less than 2 bent parts, match 2 cards, match successfuly.

**Illegal Examples**
![Alt text](Project1_Code/data/example4.png)
Although two cards are same, there is no path to connect two of them with 3 lines. In this case, two cards cannot be discarded.

**Combo**

During the game, if a player matchs and discards cards consequently without any mistake, combo score increases. 
There are some cases that can reset your combo score to be zero

    1. Choose unmatched card pair.
    2. Cancel to choose after you already choose one card.

**Goal**

The main goal of Catch & Match is to clear all of the cards except obstacles, and accumulate as much combos as players can do.

-----

## Unique Function

### Combo
![Alt text](Project1_Code/data/comboexample.png)
To detect how player play well, game gives combo score feedback to focus on the game deeply.

### Cutomizing
![Alt text](Project1_Code/data/custom.png)
Players can cutomize for their stages to control card's number. Also, to control level of difficulty, players can insert obstacles as many they want. Red number on obstacle part means that total number of cards becaome odd, so that never be cleard, so you need to change number of obsacles. Game also prevents Players not to start game on that situation.

-----
# Codes

Sorted by classes distribution

## Main function : Project1_Code

#### Parameters

ArrayList Animation **ani** : array for animation array. For memory, game draw animation and clear it before next draw

final float **CARD_IMAGE_XY_RATIO**=431.0/618, 
final float **CARD_IMAGE_XX_RATIO**=375.0/431, 
final float **CARD_IMAGE_YY_RATIO**=563.0/618 : card image's real upper face size is 375 * 563 and card's real image full size is 431 * 618. Difference because of card's side, real card image and image for upper face have different size is appeared.

final float **POSITION_RATIO_X**=0.93,
final float **POSITION_RATIO_Y**=0.96 : To make cards to be located adjacently.

int **time**=0 : to draw combo box and its materials

void **mousePressed**() : seperate GoGame state and others




## class **Animation**

#### Parameters

opacityFactor,startTime, msDuration, increase : control opacity change


opacity : opacity value

#### functions

public void run(), easeQuadraticInOut : control opacity change values

#### sub Classese

class **CorrectAnimation** extends Animation : Animation for correct match

class **WrongAnimation** extends Animation : Animation for wrong match

class **ClearAnimation** extends Animation : Animation when you success to clear stage

## class **Factory**

#### functions

Animation **newAni** : to judge whether this function is for CorrectAnimation or WrongAnimation, conduct it

Animation **clearAni** : conduct ClearAnimation

## class **RowCol**
@@@@@@@@@@@IMPORTANT@@@@@@@@@@@@@
#### Parameters

**row, col** : Actually, the right defined row should be for vertical one and col chould be for horizontal one. However I did a mistake to switch it.
So my code's row represents horizontal number, x ... and col represents vertical number, y ... 

#### functions

boolean **isSame**(RowCol rc): return whether rowcol rc is same as *this* or not 

RowCol **add**(RowCol rc), **subtract**(RowCol rc) : add / subtract with rc and return

## class **Game**

#### Parameters

int **setupVal** : to judge whether this function is for CorrectAnimation or WrongAnimation, conduct it

#### functions

void **nextStep**(Game step) : to go next state step

void **buttonIn**(int x, int y) : return where clicked

### Subclass >> **STATES**

All implements to GameState

#### class GameStart

![Alt text](Project1_Code/data/P1/1.png)
Starting page, background music start, gamestart button to go to Level state exist.

#### class Level

![Alt text](Project1_Code/data/P1/2.png)
Level setting state. When player chooses one of Easy, Medium, Hard, already set stage starts and go on a GoGame state. Otherwise, go on a LevelCostomize state.

#### class LevelCostomize

![Alt text](Project1_Code/data/P1/3.png)
To adjust card number for row and column, and the number of obsatacles, customize stages. Each triangles worked as buttons. If total number of cards are odd, game induce players to adjust obstacles number to make real cards number to be even.

#### class Loading

![Alt text](Project1_Code/data/P1/4.png)
While load to stage, games induce players to recognize game rule. When loading bar reach right, game start

#### class GoGame

![Alt text](Project1_Code/data/P1/5.png)
Main game conducting state, restart button on right make state to be StartGame. If board cleared, state changes to Clear.

#### class Clear

![Alt text](Project1_Code/data/P1/6.png)
Celebrate! Restart button on right make state to be StartGame.

## class **Match**
for isMatch() function in class Board, I made class only for returning.

#### Parameters

protected RowCol **curved1**,**curved2** : where it curves during route

 protected boolean **isMatch** : whether card pair match or not


## class **Board**

#### Parameters

int **BOARD_X**, **BOARD_Y**: real board size where cards locate.

private RowCol **selectedCard** : for calculation when muse clicked

private PVector **origin** : where real board start.

private int[][] **stateArray** : to know whether 2 cards are matched or not, each cards have state.  Since, routes pass above, below, and side to cards, I needed additional length for each. So total length for stateArray is row+2 * col+2

0 : blanked   , 1: obsacle ,2:filled ordinary card, 3: selected card

private int[][] **picData** : to know which picture does card has

#### Functions

public boolean **isAllEmpty**() : check board is empty or not, in other words, check wether the stage is cleared or not

public void **setRandomData**(int obs) : to assign picture randomly and able to clear, get organized random int array.

public Match **isMatch**(RowCol start, RowCol end) : MOST IMPORTANT CODE for this game. Return whether 2 cards are matched or not.

public boolean **isDataMatch**(RowCol start, RowCol end) : whether 2 cards have same picture or not

private RowCol **coordToRowCol** (int x, int y) : get mouse's X and Y location and convert it to row col value for grid.

public void **makeSelection** (int x, int y) : when mouse clicked, detect which card clicked and change states for effected cards.

## class **Piece**

#### Parameters

PVector **size** : x,y size

PImage **imgCard**, **imgPic** : imgCard is image for blank card, and imgPic is image for picture

int **dataPic** : which picture contains

RowCol **rowcol**, PVector **loc** : rowcol is (x,y) for grid, and loc is real x,y values

public PVector **getLocCenter**() : return center of card  (not image's center)


-----
# Library

## Minim

Use it to create sound effect. This game has background music and sound effect for clicking

import ddf.minim.*;

-----
# Inspiration
http://mideumshin.org/index.php?title=ClassMinim







