class Factory {
        Animation newAni(PVector po1,PVector po2,PVector po3,PVector po4,PVector po5, float d,long ms){
            if(po4.x<0) return new WrongAnimation(po1,po2,po3,d,ms);
            else return new CorrectAnimation(po1,po2,po3,po4,po5,d,ms);
        }
        Animation clearAni(PVector p1, float size, float opacityFactor, long msDuration){
            return new ClearAnimation (p1, size, opacityFactor, msDuration);
        }
}