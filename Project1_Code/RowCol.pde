public class RowCol
{
	private int row;
    private int col;
	public RowCol (int r, int c){
		row=r; 
        col=c;
	}

	public int getRow(){ 
        return row; 
        }
    public void setRow(int row){ 
        this.row=row; 
        }
    public int getCol(){ 
        return col; 
        }
    public boolean isSame(RowCol rc){
        return (row==rc.getRow() && col==rc.getCol());
    }
    public RowCol add(RowCol rc){
        return new RowCol(row+rc.getRow(),col+rc.getCol());
    }
    public RowCol subtract(RowCol rc){
        return new RowCol(row-rc.getRow(),col-rc.getCol());
    }
}