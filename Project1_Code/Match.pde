public class Match{
    protected RowCol curved1;
    protected RowCol curved2;
    protected boolean isMatch;
	public Match (RowCol c1, RowCol c2, boolean m){
		curved1=c1;
        curved2=c2;
        isMatch=m;
	}
    public RowCol getCurved1(){return curved1;}
    public RowCol getCurved2(){return curved2;}
    public boolean getIsMatch(){return isMatch;}
}