public class Piece{
    
    private int state;
	PVector size;
	PImage imgCard;
	PImage imgPic;
	int dataPic;
	RowCol rowcol;
    PVector loc;
    PVector origin;
    public Piece (PVector origin,RowCol rowcol, PVector size, int state,int dataPic)
	{   
        setSize (size);	
        loadCardImg();
		loadPicImg("dataPic"+dataPic+".png");
        this.rowcol=new RowCol(rowcol.row,rowcol.col);
		setRowcol (rowcol);	
        this.origin=origin;
        loc=new PVector(origin.x+rowcol.row*size.x*POSITION_RATIO_X,
                            origin.y+rowcol.col*size.y*POSITION_RATIO_Y);
        this.state=state;
		this.dataPic=dataPic;
	}
    public PVector getLocCenter(){
        // 183*277 > center
        // card image real size 375 * 563
        // card image full size 431 * 618
        return new PVector(loc.x+size.x*183.0/431,loc.y+size.y*277.0/618);
    }
    public boolean isEmpty(){
        return state==0;
    }
    public void setState (int state){
        this.state=state;
        }
    public int getState (){
        return state;
        }
    public void setRowcol (RowCol rc){
		rowcol= rc;
	}
	public void setRowcol (int x, int y){
		rowcol= new RowCol (x,y);
	}

    public void setSize (PVector size){
		this.size= size;
	}
	public void setSize (float x, float y){
		size= new PVector (x,y);
	}

    protected void loadCardImg (){
		imgCard= loadImage("cardBlank.png");
	}
	protected void loadPicImg (String pictureName){
		imgPic= loadImage(pictureName);
         imgPic.resize(int(size.x/1.4),0);
	}
    

    public void drawPicture(){
        
       
        PVector centerXY=getLocCenter();
        imageMode(CENTER);
        pushMatrix();
        image(imgPic,centerXY.x,centerXY.y);
        popMatrix();
    }
    public void draw ()
	{
        imageMode(CORNER);
		if (state==0) return;
        imgCard.resize(int(size.x), int(size.y));
        
		image (imgCard, loc.x,loc.y, size.x, size.y);
        if(state==3){
            PImage selected=loadImage("selected.png");
            image (selected, loc.x,loc.y, size.x*CARD_IMAGE_XX_RATIO, size.y*CARD_IMAGE_YY_RATIO);
        }
        drawPicture();
        
	}
}