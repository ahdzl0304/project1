public class Board{
    public static final float BOARD_X=1140;
    public static final float BOARD_Y=570;
    
    private RowCol selectedCard;
    private Piece [][] board;
    private RowCol cardNum;
    private PVector cardSize;
    private PVector origin;
    private int[][] stateArray;
	private int[][] picData;
	private int picNum;
    private int obstacle;
    private int combo;
    PImage comboBox;
    PImage comboCircle;
    PImage comboText;
    int angle=0;
    public Board(int row,int col,int picNum,int obs){
        board= new Piece [row][col];
        cardNum=new RowCol(row,col);
		this.picNum=picNum;
        float cardW=BOARD_X/row;
        float cardH=BOARD_X/col;  
        selectedCard=new RowCol(-1,-1);
        stateArray=new int[row+2][col+2];
		picData=new int[row][col];
        obstacle=obs;
        combo=0;
        for(int i=0;i<row+2;i++){
            for(int j=0;j<col+2;j++) {
				stateArray[i][j]=0;
				if(i<row && j<col) picData[i][j]=0;}
        	}
        if(BOARD_X/BOARD_Y>=float(row)*CARD_IMAGE_XY_RATIO/col){
        	cardH=(BOARD_Y)/col;
        	cardW=cardH*CARD_IMAGE_XY_RATIO;
       	}
		else{
			cardW=(BOARD_X)/row;
			cardH=cardW/CARD_IMAGE_XY_RATIO;
		}
        cardSize=new PVector(cardW,cardH);
        
        float moveToCenterX=width/2-cardSize.x*row*POSITION_RATIO_X/2;
        float moveToCenterY=height/2-cardSize.y*col*POSITION_RATIO_Y/2;
        origin=new PVector(moveToCenterX,moveToCenterY); 
        comboBox=loadImage("combobox.png");
        comboCircle=loadImage("combocircle.png");
        comboText=loadImage("combotext.png");
    }
    private Piece board(RowCol rc){
        return board[rc.row][rc.col];
    }
    public boolean isAllEmpty(){
        boolean empty=true;
        for(int i=1;i<stateArray.length-1;i++){
            for(int j=1;j<stateArray[0].length-1;j++){
                if(stateArray[i][j]==2) return false;
            }
        }
        return true;
    }
	public void setRandomData(int obs){
		IntList randomHalf=new IntList();
		for(int i=0;i<(cardNum.getRow()*cardNum.getCol())/2;i++){
            if(i<obs/2) randomHalf.append(0); 
            else randomHalf.append((int)random(1,picNum+1));
		}
		for(int i=0;i<(cardNum.getRow()*cardNum.getCol())/2;i++){
			randomHalf.append(randomHalf.get(i));
		}
        if(cardNum.getRow()*cardNum.getCol()%2==1) randomHalf.append(0);
		randomHalf.shuffle();
		int c=0;
		for(int i=0;i<cardNum.getRow();i++){
			for(int j=0;j<cardNum.getCol();j++){
				picData[i][j]=randomHalf.get(c);
				c++;
			}
		}
		
	}
    public PVector getOrigin(){
        return origin;
    }
    public PVector getSize(){
        return cardSize;
    }
    public int stateArray(RowCol rc){
        return stateArray[rc.getRow()+1][rc.getCol()+1];
    }
    public void setStateArray(RowCol rc,int state){
        stateArray[rc.getRow()+1][rc.getCol()+1]=state;
    }
    public void setStateArray(int r,int c,int state){
        stateArray[r+1][c+1]=state;
    }
    public void drawCombo(){
        if(time<=0)time=0;
        tint(255,time);
        imageMode(CENTER);
        image(comboBox,width-comboBox.width/2,height-comboBox.height/2);
        pushMatrix();
        translate(width-comboBox.width/2-100,height-comboBox.height/2);
        rotate((angle++)%(2*PI));
        image(comboCircle,0,0);
        popMatrix();
        fill(0,0,0,time);
        myFont = createFont("ab.ttf", 80);
        textFont(myFont);
        textAlign(CENTER, CENTER);
        text(combo,width-comboBox.width/2-100,height-comboBox.height/2-10);
        image(comboText,width-comboBox.width/2+50,height-comboBox.height/2+60);
        time-=10;
        
    }
    public void draw(){
        
        noStroke();
        for(int r=0;r<cardNum.row;r++){
            for(int c=0;c<cardNum.col;c++){
                    board[r][c].draw();
            }
        }
        drawCombo();
        for (Animation a: ani) a.draw();
		//drawLine();
		
    }
    public void setupBoard(){
        setRandomData(obstacle);
        for(int r=0;r<cardNum.row;r++){
            for(int c=0;c<cardNum.col;c++){
				if(picData[r][c]>0){
                    setPieceAt(new Piece(origin,new RowCol(r,c), cardSize,2
                                ,picData[r][c]),new RowCol(r,c));
                    setStateArray(r,c,2);
                }
                else if(picData[r][c]==0){
                    setPieceAt(new Piece(origin,new RowCol(r,c), cardSize,1
                                ,picData[r][c]),new RowCol(r,c));
                    setStateArray(r,c,1);
                }
            }
        }
    }
    
    //ㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁIMPORTANT
    public Match isMatch(RowCol start, RowCol end){
        int curved=0;
        boolean checked=false;
        int rightDirX=(end.getRow()>start.getRow()?1:(end.getRow()==start.getRow()?0:-1));
        int rightDirY=(end.getCol()>start.getCol()?1:(end.getCol()==start.getCol()?0:-1));
        println("rightDirX : " + rightDirX+" rightDirY : "+rightDirY);
        RowCol dirX=new RowCol(rightDirX,0);
        RowCol dirY=new RowCol(0,rightDirY);
        RowCol swim;
        int matchVal=0;
        //2 cards on same row
        if(rightDirY==0){
            int checkall=0;
            for(int i=start.getCol();i<stateArray[0].length-1;i++){
                int check=0;
                swim=start;
                for(int j=start.getCol();j<i;j++){
                    swim=swim.add(new RowCol(0,1));
                    println("rrswim1 : "+swim.getRow()+" "+swim.getCol());
                    if(stateArray(swim)!=0) {
                        check++; break;
                    }   
                }
                if(check==0){
                    for(int j=start.getRow();j!=end.getRow();j+=rightDirX){
                        swim=swim.add(dirX);
                        println("rrswim2 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0 && !swim.isSame(end)) {
                            check++; break;
                        }
                    }
                }
                if(check==0){
                    for(int j=i;j>end.getCol();j--){
                        swim=swim.subtract(new RowCol(0,1));
                        println("rrswim3 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0 && !swim.isSame(end)) {
                            check++; break;
                        }   
                    }
                }
                if(check==0) {
                    checkall++;
                    matchVal=i;
                    break;
                }
            }
            if(checkall!=0) {println("first : "+start.getRow()+","+matchVal+" / second : "+end.getRow()+","+matchVal);
                return new Match( new RowCol(start.getRow(),matchVal),new RowCol(end.getRow(),matchVal), true);
                }
            else{
                for(int i=start.getCol();i>=0;i--){
                    int check=0;
                    swim=start;
                    for(int j=start.getCol();j>=i;j--){
                        swim=swim.subtract(new RowCol(0,1));
                        println("rrswim1 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0) {
                            check++; break;
                        }   
                    }
                    if(check==0){
                        for(int j=start.getRow();j!=end.getRow();j+=rightDirX){
                            swim=swim.add(dirX);
                            println("rrswim2 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }    
                        }
                    }
                    if(check==0){
                        for(int j=i;j<=end.getCol();j++){
                            swim=swim.add(new RowCol(0,1));
                            println("rrswim3 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }   
                        }
                    }
                    if(check==0) {
                        checkall++;
                        matchVal=i-1;
                        break;
                    }
                }
                if(checkall!=0){ 
                    println("2first : "+start.getRow()+","+matchVal+" / second : "+end.getRow()+","+matchVal);
                    return new Match( new RowCol(start.getRow(),matchVal),new RowCol(end.getRow(),matchVal), true);
                }
                else return new Match( new RowCol(0,0),new RowCol(0,0), false);
            }
        }

        //2 cards on same col
        else if(rightDirX==0){
            int checkall=0;
            for(int i=start.getRow();i<stateArray.length-1;i++){
                int check=0;
                swim=start;
                for(int j=start.getRow();j<i;j++){
                    swim=swim.add(new RowCol(1,0));
                    println("rrswim1 : "+swim.getRow()+" "+swim.getCol());
                    if(stateArray(swim)!=0) {
                        check++; break;
                    }   
                }
                if(check==0){
                    for(int j=start.getCol();j!=end.getCol();j+=rightDirY){
                        swim=swim.add(dirY);
                        println("rrswim2 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0 && !swim.isSame(end)) {
                            check++; break;
                        }
                    }
                }
                if(check==0){
                    for(int j=i;j>end.getRow();j--){
                        swim=swim.subtract(new RowCol(1,0));
                        println("rrswim3 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0 && !swim.isSame(end)) {
                            check++; break;
                        }   
                    }
                }
                if(check==0) {
                    checkall++;
                    matchVal=i;
                    break;
                }
            }
            if(checkall!=0) {
                println("2first : "+matchVal+","+start.getCol()+" / second : "+matchVal+","+end.getCol());
                return new Match( new RowCol(matchVal,start.getCol()),new RowCol(matchVal,end.getCol()), true);
                }
            else{
                for(int i=start.getRow();i>=0;i--){
                    int check=0;
                    swim=start;
                    for(int j=start.getRow();j>=i;j--){
                        swim=swim.subtract(new RowCol(1,0));
                        println("rroswim1 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0) {
                            check++; break;
                        }   
                    }
                    if(check==0){
                        for(int j=start.getCol();j!=end.getCol();j+=rightDirY){
                            swim=swim.add(dirY);
                            println("rroswim2 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }                    
                        }
                    }
                    if(check==0){
                        for(int j=i;j<=end.getRow();j++){
                            swim=swim.add(new RowCol(1,0));
                            println("rroswim3 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }   
                        }
                    }
                    if(check==0) {
                        checkall++;
                        matchVal=i-1;
                        break;
                    }
                }
                if(checkall!=0) {
                println("2first : "+matchVal+","+start.getCol()+" / second : "+matchVal+","+end.getCol());
                return new Match( new RowCol(matchVal,start.getCol()),new RowCol(matchVal,end.getCol()), true);
                }
                else return new Match( new RowCol(0,0),new RowCol(0,0), false);
            }
        }
        //2 cards on differend col and row
        else{
            //ㅣ                   ㅣ
            //ㅣ            ㅡㅡㅡㅡ
            //  ㅡㅡㅡㅡ   ㅣ
            //         ㅣ  ㅣ          shape connection lines
            int checkall=0;
            for(int i=start.getCol();i!=end.getCol();i+=rightDirY){
                int check=0;
                swim=start;
                for(int j=start.getCol();j!=i;j+=rightDirY){
                    swim=swim.add(dirY);
                    println("rcCswim1 : "+swim.getRow()+" "+swim.getCol());
                    if(stateArray(swim)!=0) {
                        check++; break;
                    }   
                }
                if(check==0){
                    for(int j=start.getRow();j!=end.getRow();j+=rightDirX){
                        swim=swim.add(dirX);
                        println("rcCswim2 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0 && !swim.isSame(end)) {
                            check++; break;
                        }
                    }
                }
                if(check==0){
                    for(int j=i;j!=end.getCol();j+=rightDirY){
                        swim=swim.add(dirY);
                        println("rCcswim3 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0 && !swim.isSame(end)) {
                            check++; break;
                        }   
                    }
                }
                if(check==0) {
                    checkall++;
                    matchVal=i;
                    break;
                }
            }
            if(checkall!=0) {println("first : "+start.getRow()+","+matchVal+" / second : "+end.getRow()+","+matchVal);
                return new Match( new RowCol(start.getRow(),matchVal),new RowCol(end.getRow(),matchVal), true);
            }
            else{
                //ㅡㅡ             ㅡㅡ
                //   ㅣ           ㅣ
                //   ㅣ           ㅣ
                //     ㅡㅡ    ㅡㅡ       shape connection lines

                checkall=0;
                for(int i=start.getRow();i!=end.getRow();i+=rightDirX){
                    int check=0;
                    swim=start;
                    for(int j=start.getRow();j!=i;j+=rightDirX){
                        swim=swim.add(dirX);
                        println("rcCcswim1 : "+swim.getRow()+" "+swim.getCol());
                        if(stateArray(swim)!=0) {
                            check++; break;
                        }   
                    }
                    if(check==0){
                        for(int j=start.getCol();j!=end.getCol();j+=rightDirY){
                            swim=swim.add(dirY);
                            println("rcCcswim2 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }
                        }
                    }
                    if(check==0){
                        for(int j=i;j!=end.getRow();j+=rightDirX){
                            swim=swim.add(dirX);
                            println("rcCcswim3 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }   
                        }
                    }
                    if(check==0) {
                        checkall++; 
                        matchVal=i;
                        break;
                    }
                }
            }
            if(checkall!=0) {
                println("2first : "+matchVal+","+start.getCol()+" / second : "+matchVal+","+end.getCol());
                return new Match( new RowCol(matchVal,start.getCol()),new RowCol(matchVal,end.getCol()), true);
            }
            
            else{
                // ㅣ                   ㅣ   ㅡㅡㅡㅡ   ㅡㅡㅡㅡ
                // ㅣ      ㅣ  ㅣ       ㅣ  ㅣ      ㅣ ㅣ      ㅣ
                //  ㅡㅡㅡㅡ     ㅡㅡㅡㅡ           ㅣ ㅣ          shape connection lines
                checkall=0;
                for(int i=end.getCol();(i!=stateArray[0].length-1 && i!=-2);i+=rightDirY){
                    int check=0;
                    swim=start;
                    for(int j=start.getCol();j!=i;j+=rightDirY){
                        swim=swim.add(dirY);
                        println("rr11swim1 : "+swim.getRow()+" "+swim.getCol()+" / "+i);
                        if(stateArray(swim)!=0) {
                            check++; break;
                        }   
                    }
                    if(check==0){
                        for(int j=start.getRow();j!=end.getRow();j+=rightDirX){
                            swim=swim.add(dirX);
                            println("rr12swim2 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }
                        }
                    }
                    if(check==0){
                        for(int j=i;j!=end.getCol();j-=rightDirY){
                            swim=swim.subtract(dirY);
                            println("rr13swim3 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }   
                        }
                    }
                    if(check==0) {
                        checkall++;
                        matchVal=i;
                        break;
                    }
                }
                if(checkall!=0) {println("first : "+start.getRow()+","+matchVal+" / second : "+end.getRow()+","+matchVal);
                    return new Match( new RowCol(start.getRow(),matchVal),new RowCol(end.getRow(),matchVal), true);
                    }
                else{
                    checkall=0;
                    for(int i=start.getCol();i!=stateArray[0].length-1 && i!=-2;i-=rightDirY){
                        int check=0;
                        swim=start;
                        for(int j=start.getCol();j!=i;j-=rightDirY){
                            swim=swim.subtract(dirY);
                            println("rr21swim1 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0) {
                                check++; break;
                            }   
                        }
                        if(check==0){
                            for(int j=start.getRow();j!=end.getRow();j+=rightDirX){
                                swim=swim.add(dirX);
                                println("rr22swim2 : "+swim.getRow()+" "+swim.getCol());
                                if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                    check++; break;
                                }    
                            }
                        }
                        if(check==0){
                            for(int j=i;j!=end.getCol();j+=rightDirY){
                                swim=swim.add(dirY);
                                println("rr23swim3 : "+swim.getRow()+" "+swim.getCol());
                                if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                    check++; break;
                                }   
                            }
                        }
                        if(check==0) {
                            checkall++;
                            matchVal=i;
                            break;
                        }
                    }
                    
                }
            }
            if(checkall!=0){ 
                println("2first : "+start.getRow()+","+matchVal+" / second : "+end.getRow()+","+matchVal);
                return new Match( new RowCol(start.getRow(),matchVal),new RowCol(end.getRow(),matchVal), true);
            }
            //ㅡㅡㅡㅡ    ㅡ               ㅡ    ㅡㅡㅡㅡ
            //       ㅣ ㅣ                  ㅣ ㅣ
            //     ㅡ     ㅡㅡㅡㅡㅡ  ㅡㅡㅡㅡ   ㅡ          shape connection lines
            else{
                checkall=0;
                for(int i=end.getRow();(i!=stateArray.length-1 && i!=-2);i+=rightDirX){
                    int check=0;
                    swim=start;
                    for(int j=start.getRow();j!=i;j+=rightDirX){
                        swim=swim.add(dirX);
                        println("rr11swim1 : "+swim.getRow()+" "+swim.getCol()+" / "+i);
                        if(stateArray(swim)!=0) {
                            check++; break;
                        }   
                    }
                    if(check==0){
                        for(int j=start.getCol();j!=end.getCol();j+=rightDirY){
                            swim=swim.add(dirY);
                            println("rr12swim2 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }
                        }
                    }
                    if(check==0){
                        for(int j=i;j!=end.getRow();j-=rightDirX){
                            swim=swim.subtract(dirX);
                            println("rr13swim3 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                check++; break;
                            }   
                        }
                    }
                    if(check==0) {
                        checkall++;
                        matchVal=i;
                        break;
                    }
                }
                if(checkall!=0) {println("first : "+matchVal+","+start.getCol()+" / second : "+matchVal+","+end.getCol());
                    return new Match( new RowCol(matchVal,start.getCol()),new RowCol(matchVal,end.getCol()), true);
                    }
                else{
                    checkall=0;
                    for(int i=start.getRow();i!=stateArray.length-1 && i!=-2;i-=rightDirX){
                        int check=0;
                        swim=start;
                        for(int j=start.getRow();j!=i;j-=rightDirX){
                            swim=swim.subtract(dirX);
                            println("rr21swim1 : "+swim.getRow()+" "+swim.getCol());
                            if(stateArray(swim)!=0) {
                                check++; break;
                            }   
                        }
                        if(check==0){
                            for(int j=start.getCol();j!=end.getCol();j+=rightDirY){
                                swim=swim.add(dirY);
                                println("rr22swim2 : "+swim.getRow()+" "+swim.getCol());
                                if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                    check++; break;
                                }    
                            }
                        }
                        if(check==0){
                            for(int j=i;j!=end.getRow();j+=rightDirX){
                                swim=swim.add(dirX);
                                println("rr23swim3 : "+swim.getRow()+" "+swim.getCol());
                                if(stateArray(swim)!=0 && !swim.isSame(end)) {
                                    check++; break;
                                }   
                            }
                        }
                        if(check==0) {
                            checkall++;
                            matchVal=i;
                            break;
                        }
                    }
                    
                }
            }
            if(checkall!=0){ 
                println("2first : "+matchVal+","+start.getCol()+" / second : "+matchVal+","+end.getCol());
                return new Match( new RowCol(matchVal,start.getCol()),new RowCol(matchVal,end.getCol()), true);
            }
            else return new Match( new RowCol(0,0),new RowCol(0,0), false);
        } 
    }
        //return new Match( new RowCol(0,0),new RowCol(0,0), false);
    public boolean isDataMatch(RowCol start, RowCol end){
		return picData[start.getRow()][start.getCol()]==picData[end.getRow()][end.getCol()];
	}

    private RowCol coordToRowCol (int x, int y)
	{
        float xVal=(x-origin.x)/(cardSize.x*POSITION_RATIO_X);
        float yVal=(y-origin.y)/(cardSize.y*POSITION_RATIO_Y);
        int xReturn;
        int yReturn;
        if (xVal>=0){xReturn=int(xVal);}
        else {xReturn=int(xVal)-1;}
        if (yVal>=0){yReturn=int(yVal);}
        else {yReturn=int(yVal)-1;}
		return new RowCol (xReturn,yReturn);
	}

    private void setPieceAt (Piece p, RowCol rc){
        //if (inBounds(rc))
			board[rc.getRow()][rc.getCol()]= p;
	}
    private boolean isBound(RowCol rc){
        if(rc.getRow()<0 || rc.getRow()>=cardNum.getRow() || rc.getCol()<0 || rc.getCol()>=cardNum.getCol()) return false;
        return true;
    }
    public void makeSelection (int x, int y){
        RowCol newCard = coordToRowCol(x, y);
        if(!isBound(newCard)) return;
        
        else{
            if(selectedCard.getRow()==-1 && board(newCard).getState()==2){
              selectedCard=newCard;
              board(newCard).setState(3);
              setStateArray(newCard,3);
            }
            else if(selectedCard.getRow()==-1 && board(newCard).getState()==1){  }
            else if(selectedCard.getRow()==-1 && board(newCard).getState()==0){  }
            else if(board(newCard).getState()!=0 && !newCard.isSame(selectedCard) && isMatch(selectedCard, newCard).getIsMatch() && isDataMatch(selectedCard, newCard)){
				PVector po1=new Piece(origin,new RowCol(selectedCard.getRow(),selectedCard.getCol()), cardSize, 2 ,0).getLocCenter();
				PVector po2=new Piece(origin,isMatch(selectedCard, newCard).getCurved1(), cardSize, 2,0 ).getLocCenter();
				PVector po3=new Piece(origin,isMatch(selectedCard, newCard).getCurved2(), cardSize, 2 ,0).getLocCenter();
				PVector po4=new Piece(origin,newCard, cardSize, 2,0).getLocCenter();
				ani.clear();
                ani.add (fac.newAni(po1,po2,po3,po4,cardSize, 4, 200));
				board(selectedCard).setState(0);
                setStateArray(selectedCard,0);
                board(newCard).setState(0);
                setStateArray(newCard,0);
                selectedCard.setRow(-1);
                combo++;
                time=255;
            }
            else if(board(newCard).getState()!=0 && !newCard.isSame(selectedCard) && !isMatch(selectedCard, newCard).getIsMatch() ){
                PVector po1=new Piece(origin,new RowCol(selectedCard.getRow(),selectedCard.getCol()), cardSize, 2 ,0).getLocCenter();
                PVector po2=new Piece(origin,newCard, cardSize, 2,0).getLocCenter();
                ani.clear();
                ani.add (fac.newAni(po1,po2,cardSize,new PVector(-1,-1),new PVector(-1,-1), 4, 200));
                
                board(selectedCard).setState(2);
                setStateArray(selectedCard,2);
                selectedCard.setRow(-1);
                println("wrong pair!");
                combo=0;
                
				
            }
			else if(board(newCard).getState()!=0 && !newCard.isSame(selectedCard) && !isDataMatch(selectedCard, newCard)){
                PVector po1=new Piece(origin,new RowCol(selectedCard.getRow(),selectedCard.getCol()), cardSize, 2 ,0).getLocCenter();
                PVector po2=new Piece(origin,newCard, cardSize, 2,0).getLocCenter();
                ani.clear();
                ani.add (fac.newAni(po1,po2,cardSize,new PVector(-1,-1),new PVector(-1,-1), 4, 200));

                println("different picture!"+" first : "+board(selectedCard).dataPic+","+picData[selectedCard.getRow()][selectedCard.getCol()]+" / second : "+board(newCard).dataPic+","+picData[newCard.getRow()][newCard.getCol()]);
				board(selectedCard).setState(2);
                setStateArray(selectedCard,2);
                selectedCard.setRow(-1);
                combo=0;
            }
            else if(board(newCard).getState()!=0 && newCard.isSame(selectedCard)){
                board(newCard).setState(2);
                setStateArray(newCard,2);
                selectedCard.setRow(-1);
                combo=0;
            }

           
        } 
        
    }
}