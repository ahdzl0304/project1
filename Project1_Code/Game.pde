//StateMachine Start
int setupVal=0;
PFont myFont;
class Game
{
	public GameState state;
    
	void go()
	{
		setState (new GameStart());
	}
    void move(){
        state.nextStep(this);
        if(setupVal<1) state.setup();
        setupVal++;
        state.draw();
        
    }
	void setState (GameState s)
	{
		state= s;
	}
}

interface GameState
{
	void nextStep(Game step);
    void buttonIn(int x, int y);
    void draw();
    void setup();
}
//first screen
class GameStart implements GameState
{
    int whichButton=0;
    void buttonIn(int x, int y){ 
        if (x>860 && x<1200 && y>460 && y<690) whichButton=1;
        else whichButton=0;
    }
	void nextStep(Game step){
        if(whichButton==1){
		    println("--STATE----------------Gamestart ");
		    step.setState (new Level());
            whichButton=0;
            setupVal=0;
        
        }
        else {
            step.setState (new GameStart());
        }  
	}
    void draw(){
        setupBg(0);
        drawButtons(0,1000,550,300);
    }
    void setup(){
        println("call it only 1time!");
        bgMusic.play(255);
        }
    
}
//level setting screen
class Level implements GameState
{
    
    int whichButton=0;
    void buttonIn(int x, int y){ 
        println(x+" "+y+" : "+whichButton);
        //1:EASY 2:MEDIUM 3:HARD 4:CUSTOMIZED
        if (x>647 && x<810 && y>247 && y<310) whichButton=1;
        else if (x>627 && x<829 && y>328 && y<390) whichButton=2;
        else if (x>647 && x<810 && y>415 && y<477) whichButton=3;
        else if (x>548 && x<909 && y>530 && y<590) whichButton=4;
        else whichButton=0;
    }
    void nextStep(Game step){
        if(whichButton==1){
            println("--STATE----------------Level");
            board=new Board(4,4,5,0);
            step.setState (new Loading());
            whichButton=0;
            setupVal=0;
        } 
        else if(whichButton==2){
            println("--STATE----------------Level");
            board=new Board(8,6,10,4);
            step.setState (new Loading());
            whichButton=0;
            setupVal=0;
        }
        else if(whichButton==3){
            println("--STATE----------------Level");
            board=new Board(15,8,16,10);
            step.setState (new Loading());
            whichButton=0;
            setupVal=0;
        }
        else if(whichButton==4){
            println("--STATE----------------Level");
            step.setState (new LevelCostomize());
            whichButton=0;
            setupVal=0;
        }  
    }
    
    void draw(){
        setupBg(1);
        drawButtons(1,730,420,360);
    }
    void setup(){
        println("call it only 1time!");
        
        }
  
}
//level customizing screen
class LevelCostomize implements GameState
{
    
    int whichButton=0;
    int widthCust=8;
    int heightCust=5;
    int obstacles=0;
    boolean rem=false;
    color c=color(255,255,255);
    private int howManyPics(int w,int h){
        int val=w*h/5;
        if(val<2) val=2;
        else if(val>16) val=16;
        return val;
    }
    void buttonIn(int x, int y){ 
        println("levelcosto : "+x+" "+y);
        //go!!
        if (x>660 && x<805 && y>560 && y<613) whichButton=1;
        //width
        else if (x>316 && x<365 && y>218 && y<270) whichButton=2;
        else if (x>793 && x<845 && y>218 && y<270) whichButton=3;
        //height
        else if (x>228 && x<285 && y>329 && y<376) whichButton=4;
        else if (x>228 && x<285 && y>481 && y<533) whichButton=5;
        //obsacles
        else if (x>1162 && x<1218 && y>329 && y<375) whichButton=6;
        else if (x>1162 && x<1218 && y>483 && y<532) whichButton=7;

        else whichButton=0;
    }
    void nextStep(Game step){
        // 1 Go game 2 width number --  3 width number ++    4 height number ++   5 height number --    6 obstacle number ++   7 obstacle number--
        if(whichButton==1 && rem){
            println("--STATE----------------Level");
            board=new Board(widthCust,heightCust,howManyPics(widthCust,heightCust),obstacles);
            step.setState (new Loading());
            whichButton=0;
            setupVal=0;
        }
        else if(whichButton==2){
            if(widthCust>=2) widthCust--;
            whichButton=0;
        }
        else if(whichButton==3){
            if(widthCust<=19) widthCust++;
            whichButton=0;
        }
        else if(whichButton==4){
            if(heightCust<=9) heightCust++;
            whichButton=0;
        }
        else if(whichButton==5){
            if(heightCust>=2) heightCust--;
            whichButton=0;
        }
        else if(whichButton==6){
            if(obstacles<=widthCust*heightCust/2) obstacles++;
            whichButton=0;
        }
        else if(whichButton==7){
            if(obstacles>=1) obstacles--;
            whichButton=0;
        }
        if(obstacles>widthCust*heightCust/2) obstacles=widthCust*heightCust/2;
        if((widthCust*heightCust+obstacles)%2==1) {
            c=color(255,0,0);
            rem=false;
        }
        else{
            c=color(255,255,255);
            rem=true;
        }
        if(whichButton==1 && rem){
            println("--STATE----------------Level");
            board=new Board(widthCust,heightCust,howManyPics(widthCust,heightCust),obstacles);
            step.setState (new Loading());
            whichButton=0;
            setupVal=0;
        }
    }
    void draw(){
        setupBg(5);
        fill(255,255,255);
        myFont = createFont("ab.ttf", 32);
        textFont(myFont);
        textAlign(CENTER, CENTER);
        text(widthCust, 575,240);
        text(heightCust, 258,426);
        fill(c);
        text(obstacles, 1191,424);
    }
    void setup(){
        println("call it only 1time!");
        }
}
// loading session
class Loading implements GameState
{
    
    int whichButton=0;
    int loadingX=0;
    int loadingY=20;
    int speed=20;
    //for me, pass faster
    void buttonIn(int x, int y){ 
        if (x>0 && x<100 && y>100 && y<200) whichButton=1;
        else whichButton=0;
    }
    void nextStep(Game step){
        if(whichButton==1){
	        println("--STATE---------------Loading");
		    step.setState (new GoGame());
            whichButton=0;
            setupVal=0;
        }
        if(loadingX>width) whichButton=1;
	}
     void draw(){
        setupBg(2);
        noStroke();
        fill(212,54,148);
        rect(0,height-loadingY,loadingX,loadingY);
        fill(254,190,16);
        rect(0,height-loadingY*3/4,loadingX,loadingY/2);
        loadingX+=speed;
    }
    void setup(){
        board.setupBoard();
        println("call it only 1time!");
        }
}
//in game screen
class GoGame implements GameState
{
    
    int whichButton=0;
    void buttonIn(int x, int y){ 
        
        if (x>1358 && y>438 && y<563) whichButton=2;
        else whichButton=0;
    }
    
    void nextStep(Game step){
        
        if(whichButton==1){
	        println("--STATE---------------GoGame");
            step.setState (new Clear());
            whichButton=0;	
            setupVal=0;
        }
        else if(whichButton==2){
	        println("--STATE---------------GoGame");
            step.setState (new GameStart());
            whichButton=0;	
            setupVal=0;
        }
    }
    void draw(){
	    setupBg(3);
        if(board!=null) board.draw();
        PImage re=loadImage("restart.png");
        imageMode(CENTER);
        re.resize(int(re.width/2.2),0);
        image(re,width-re.width/2,height-re.height/2-150);
        if(board.isAllEmpty()) {
            whichButton=1;
        }
    }
    void setup(){println("call it only 1time!");}
}
//clear! ready to reset
class Clear implements GameState
{
    
    int whichButton=0;
    void buttonIn(int x, int y){ 
        
        println(x+" "+y+" : "+whichButton);
        if (x>540 && x<890 && y>400 && y<505) whichButton=1;
        else whichButton=0;
    }
    void nextStep(Game step){
      if(whichButton==1){
	        println("--STATE---------------Clear");
            step.setState (new GameStart());	
            whichButton=0;
            setupVal=0;
        }
    }
    
  
    void draw(){
        setupBg(4);
        drawButtons(4,720,460,360);
        delay(500);
        ani.clear();
        ani.add (fac.clearAni(new PVector (random(200,1280),random(200,520)),random(100,500),random(1,10), int(random(100,1000))));
        ani.add (fac.clearAni(new PVector (random(200,1280),random(200,520)),random(100,500),random(1,10), int(random(100,1000))));
        ani.add (fac.clearAni(new PVector (random(200,1280),random(200,520)),random(100,500),random(1,10), int(random(100,1000))));
        for (Animation a: ani) a.draw();
    }
    void setup(){println("call it only 1time!");}
}

//StateMachine End
