/*
	Project 1
	Name of Project: Catch and Match (for Midterm, (only)Match)
	Author: Hwang Youngzu
	Date: 2020-05-29
*/
//minim importing
import ddf.minim.*;
Minim minim;
Minim minimLib1;
Minim minimLib2;
AudioPlayer bgMusic;
AudioPlayer clickedMusic;
//minim import end

ArrayList<Animation> ani;
Board board;
Factory fac;
Game g=new Game();
// card image real size 375 * 563
// card image full size 431 * 618
// to test whether two cards are matched or not
final float CARD_IMAGE_XY_RATIO=431.0/618;
final float CARD_IMAGE_XX_RATIO=375.0/431;
final float CARD_IMAGE_YY_RATIO=563.0/618;
//because of card's side, real card image and image for upper face have different size 
final float POSITION_RATIO_X=0.93;
final float POSITION_RATIO_Y=0.96;
int time=0;
void setup()
{
    g.go();
    size (1480, 720);
	
    ani= new ArrayList<Animation>();
    fac= new Factory();
	ani.add (new CorrectAnimation(new PVector(0,0),new PVector(0,0),new PVector(0,0),new PVector(0,0),new PVector(10,10), 0, 1));
    
    minimLib1 = new Minim(this);
    minimLib2 = new Minim(this);
    bgMusic = minimLib1.loadFile("bg.mp3");
    clickedMusic = minimLib2.loadFile("clickedDigi.mp3");
    
}


void setupBg(int i){
    PImage img;
    img = loadImage("bg"+i+".png");
    img.resize(width, 0);
    background(img);
}
void drawButtons(int i,int x,int y,int size){
    PImage img;
    imageMode(CENTER);
    img = loadImage("button"+i+".png");
    img.resize(size, 0);
    image(img,x,y);
}

void draw(){
	g.move();
}
void mousePressed()
{
    clickedMusic.play(255);
    if(g.state instanceof GoGame){
        board.makeSelection(mouseX, mouseY);
        g.state.buttonIn(mouseX,mouseY);  
    }
    else {
        g.state.buttonIn(mouseX,mouseY);
    }

}