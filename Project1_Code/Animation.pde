abstract class Animation extends Thread{
    float opacityFactor;
    long startTime, msDuration;
	boolean increase;
	float opacity;
    Animation (float opacityFactor, long msDuration)
	{
		this.opacityFactor= opacityFactor;
		this.msDuration= msDuration;
		increase= true;
        this.opacity= 0.1;
		start();
	}
    void draw(){}
    public void run()
	{
		startTime= millis();
		while (true)
		{
			long currentTime= millis()-startTime;
			
			if (currentTime > msDuration){ increase= !increase; startTime= millis();}
			float perc= (float)currentTime/msDuration;
			
			if (!increase) perc= 1-perc;
			opacity=60*easeQuadraticInOut(perc)*(opacityFactor);
			if(opacity==0 && currentTime>10) break;
		}
	}
	float easeQuadraticInOut(float t)
	{
		float sqt= t*t;
		return sqt / (2.0f * (sqt-t) + 1.0f);
	}
}
class CorrectAnimation extends Animation
{
	PVector p1,p2,p3,p4;
    PVector cardSize;
	CorrectAnimation (PVector p1,PVector p2,PVector p3,PVector p4,PVector cardSize, float opacityFactor, long msDuration)
	{
        super(opacityFactor,msDuration);
		this.p1= p1; this.p2= p2; this.p3= p3; this.p4= p4; 
        this.cardSize=cardSize;
	}
	void draw()
	{
        strokeWeight(12);
        stroke(156,83,118,opacity/2);
		pushMatrix();
		line(p1.x,p1.y,p2.x,p2.y);
		line(p2.x,p2.y,p3.x,p3.y);
		line(p3.x,p3.y,p4.x,p4.y);
		popMatrix();

		strokeWeight(7);
        stroke(156,83,118,opacity);
		pushMatrix();
		line(p1.x,p1.y,p2.x,p2.y);
		line(p2.x,p2.y,p3.x,p3.y);
		line(p3.x,p3.y,p4.x,p4.y);
		popMatrix();
		
		strokeWeight(3);
        stroke(219,153,77,opacity);
		pushMatrix();
		line(p1.x,p1.y,p2.x,p2.y);
		line(p2.x,p2.y,p3.x,p3.y);
		line(p3.x,p3.y,p4.x,p4.y);
		popMatrix();

        fill(219,153,77,opacity);
        int radius=int(cardSize.x/5);
        pushMatrix();
        ellipseMode(CENTER);
        ellipse(p1.x,p1.y,radius,radius);
        ellipse(p4.x,p4.y,radius,radius);
        popMatrix();
        
        PImage wrongI=loadImage("firework.png");
        int gap=int(cardSize.x*0.2);
        wrongI.resize(int(cardSize.x*1.5),0);
        tint(255,opacity);
        imageMode(CENTER);
        image(wrongI,p1.x,p1.y+gap);
        image(wrongI,p4.x,p4.y+gap);
        tint(255,255);
	}	
}
class WrongAnimation extends Animation
{
	PVector p1,p2;
    PVector cardSize;
	WrongAnimation (PVector p1,PVector p2,PVector cardSize, float opacityFactor, long msDuration)
	{
        super(opacityFactor,msDuration);
        this.p1=p1;
        this.p2=p2;
        this.cardSize=cardSize;
	}
	void draw()
	{//375 * 563
        PImage wrongI=loadImage("wrong.png");
        int gap=int(cardSize.x*0.1);
        wrongI.resize(int(cardSize.x),0);
        tint(255,opacity);
        imageMode(CENTER);
        image(wrongI,p1.x+gap,p1.y+gap);
        image(wrongI,p2.x+gap,p2.y+gap);
        tint(255,255);
	}	
}

class ClearAnimation extends Animation
{
    PVector p1;
    float size;
	ClearAnimation (PVector p1,float size, float opacityFactor, long msDuration)
	{
        super(opacityFactor,msDuration);
        this.p1=p1;
        this.size=size;
	}
	void draw()
	{
        PImage wrongI=loadImage("firework.png");
        
        tint(255,opacity);
        imageMode(CENTER);
        wrongI.resize(int(size),0);
        image(wrongI,p1.x,p1.y);
        tint(255,255);
	}	
}